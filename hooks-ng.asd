
(defsystem hooks-ng
  :pathname "src"
  :serial t
  :components ((:file "package")
               (:file "hooks-ng"))
  :depends-on (alexandria
               let+
               closer-mop
               ))
