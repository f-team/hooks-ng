
(in-package hooks-ng)

(defclass hookable ()
  ((before :initform nil)
   (after :initform nil)
   (around :initform nil))
  (:metaclass funcallable-standard-class))

(defvar *hookables* nil "A list of all hookable functions")
(defvar *around-hooks* nil "Holds remaining :AROUND hooks to run")
(defvar *body* nil "Main hookable function body")
(defvar *result* nil "Stores the main body result")
(defvar *executed-main* nil "Fool-proofing for CALL-NEXT in :AROUND hooks")
(defvar *args* nil "Holds arguments for calling :AROUND hooks")

(defmacro defhookable (name args &body raw-body)
  "Defines a hookable function which basically behaves just like ordinary function."
  (let* ((docstring (when (and (> (length raw-body) 1)
                                (stringp (first raw-body)))
                       (first raw-body)))
         (body (if docstring
                    (rest raw-body)
                    raw-body)))
    (when docstring
      (setf (documentation name 'function) docstring))
    `(let* ((hookable (ensure-hookable ',name))
            (main #'(lambda ,args
                      (let ((*result* nil)
                            (*executed-main* nil)
                            (*body* #'(lambda () ,@body))
                            (*args* (list* ,@(lambda-list-to-args args))))
                        (call-hooks hookable 'before)
                        (call-around-hooks hookable)
                        (call-hooks hookable 'after)
                        *result*))))
       (set-funcallable-instance-function hookable main)
       (pushnew ',name *hookables*)
       ',name)))

(defun ensure-hookable (name)
  "Registers a hookable function if there wasn't any or if it was non-hookable
  (i.e. a macro, a generic or a simple function)"
  (let ((hookable (handler-case (symbol-function name)
                    (undefined-function () nil))))
    (if (typep hookable 'hookable)
        hookable
        (setf (symbol-function name) (make-instance 'hookable)))))

(defmacro defhook (name when hookable (&rest args) &body body)
  "Defines a hook named NAME for function HOOKABLE
  Depending on the value of WHEN the hook is ran
  :BEFORE before the main function
  :AFTER after the main function
  :AROUND around the main function. You MUST call CALL-NEXT somewhere inside

  Other than that the order of hooks is arbitrary

  The main function's result is available using the local
  function RESULT inside :AFTER and (after CALL-NEXT) :AROUND hooks

  ARGS are either NIL or a lambda list compatible to the original
  function's lambda list."
  (let ((slot (case when
                (:before 'before)
                (:after 'after)
                (:around 'around))))
    (when (and (eq when :around)
               (not (member 'call-next (flatten body))))
      (error "No CALL-NEXT found in a ~A ~A ~A hook" name when hookable))
    `(symbol-macrolet ((hooks (slot-value (ensure-hookable ',hookable) ',slot)))
       (deletef hooks ',name :key #'first)
       (push (list ',name
                   ,(not (null args))
                   #'(lambda ,args
                       (flet (,@(when (eq slot 'around)
                                      `((call-next ()
                                          "Calls next hook or the main function body
  returning (in either case) the main function body's result."
                                          (call-next%)
                                          *result*)
                                        (result () *result*)))
                              ,@(when (eq slot 'after)
                                      `((result () *result*))))
                         ,@(when (member slot '(around after))
                             '((declare (ignorable (function result)))))
                         ,@body)))
             hooks)
       ',name)))

(defun call-next% ()
  "Calls next hook in the chain or the main function"
  (if *around-hooks*
      (let+ ((((hook-name has-args func) &rest *around-hooks*) *around-hooks*))
        (if has-args
            (apply func *args*)
            (funcall func))
        (unless *executed-main*
          (error "Hook ~A didn't execute CALL-NEXT"
                 hook-name)))
      (setf *result* (funcall *body*)
            *executed-main* t)))

(defun call-hooks (hookable type)
  "Calls :BEFORE and :AFTER hooks"
  (dolist (hook (slot-value hookable type))
    (let+ (((_ has-args func) hook))
      (if has-args
          (apply func *args*)
          (funcall func)))))

(defun call-around-hooks (hookable)
  "Establishes dynamic vars required to run :AROUND hooks.
  Actual running is done with CALL-NEXT%"
  (let ((*around-hooks* (slot-value hookable 'around)))
    (call-next%)))

(defmacro remhook (name when hookable)
  "Removes a hook identified by pair NAME and WHEN from function HOOKABLE"
  (let ((slot (case when
                (:before 'before)
                (:after 'after)
                (:around 'around))))
    `(progn
       (deletef (slot-value (ensure-hookable ',hookable) ',slot)
                ',name
                :key #'first)
       ',name)))

(defun all-hookables ()
  "Returns the list of all hookable functions in the system"
  *hookables*)

;;;; Arguments handling and call forwarding

(defun lambda-list-to-args (lambda-list)
  "Makes a list of arguments for APPLY to forward a function call"
  (let+ (((:values required optional rest key _ _ _)
          (parse-ordinary-lambda-list lambda-list)))
    (append required
            (mapcar #'first optional)
            (if rest
                (list rest)
                (append (mapcan #'first key) (list nil))))))
