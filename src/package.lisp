
(defpackage hooks-ng
  (:use :c2cl
        :alexandria
        :let+)
  (:export #:defhookable
           #:defhook
           #:remhook

           ;; local functions
           #:result
           #:call-next

           ;; "Introspection"
           #:all-hookables))
