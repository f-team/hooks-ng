
(defpackage hooks-ng.examples
  (:use :cl
        :hooks-ng)
  (:export #:example
           #:example2))

(in-package hooks-ng.examples)

(defhookable example (a)
  "Look! A docstring!"
  (format t "~A => ~A @ Main!~%" a (1+ a))
  (1+ a))

(defhook example-hook :before example (a)
  (format t "~A @ Before!~%" a))

(defhook example-hook :around example ()
  (format t "Around!~%")
  (format t "Main function returned ~A in Around~%" (call-next))
  nil)

(defhook example-hook2 :around example ()
  (format t "Around2!~%")
  (format t "Main function returned ~A in Around2~%" (call-next))
  nil)

(defhook example-hook :after example ()
  (format t "Result is ~A After!~%" (result)))

#+(or)
(defhook example-fail :around example ()
  (format t "No CALL-NEXT here!~%"))

(defhookable example2 ()
  "Should fail because some :AROUND hook doesn't call CALL-NEXT"
  nil)

(defhook around1 :around example2 ()
  (format t "1~%")
  (call-next))

(defhook around2 :around example2 ()
  (format t "2~%")
  (when nil ; Let's try to fool the system
    (call-next)))

(defhook around3 :around example2 ()
  (format t "3~%")
  (call-next))
